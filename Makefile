REGISTRY = registry.gitlab.com
IMAGE_TAG = ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}

user:
	@protoc --proto_path=./proto/ --proto_path=./proto/third_party --go_out=plugins=grpc:/home/dev/work/src user/user.proto

order:
	@protoc --proto_path=./proto/ --proto_path=./proto/third_party --go_out=plugins=grpc:/home/dev/work/src order/order.proto

product:
	@protoc --proto_path=./proto/ --proto_path=./proto/third_party --go_out=plugins=grpc:/home/dev/work/src product/product.proto

service:
	@protoc --proto_path=./proto/ --proto_path=./proto/third_party --go_out=plugins=grpc:/home/dev/work/src service.proto

build:
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod=vendor -o /builds/${CI_PROJECT_PATH}/build/shophobe-grpc

docker-build:
	docker build -t ${IMAGE_TAG} . && \
	docker login -u cyantarek -p ${DOCKER_PASS} ${REGISTRY} && \
	docker push ${IMAGE_TAG}