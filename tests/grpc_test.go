package tests

import (
	"context"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/cyantarek/shophobe-grpc-tast/db"
	"gitlab.com/cyantarek/shophobe-grpc-tast/handlers"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto/order"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto/product"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
	"log"
	"net"
	"os"
	"testing"
	"time"
)

var client proto.ServiceClient
var lis *bufconn.Listener
var conn *grpc.ClientConn
//var mDB *mocks.DB

// The DB is not mocked right now. So make sure to run redis inside a docker container
// or anywhere you wish. Just put the host and port below
// or set them, through environment variables
var (
	DB_HOST="172.17.0.2"
	DB_PORT="6379"
)

func init() {
	// create a new buffered listener
	lis = bufconn.Listen(1024*1024)

	// initialize grpc server
	srv := grpc.NewServer()

	// initialize mock db
	//mDB = new(mocks.DB)

	// initialize redis db
	if os.Getenv("DB_HOST") != "" {
		DB_HOST = os.Getenv("DB_HOST")
	}

	if os.Getenv("DB_PORT") != "" {
		DB_PORT = os.Getenv("DB_PORT")
	}
	dbs, err := db.NewRedisDb(os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT"), os.Getenv("DB_PASS"))
	if err != nil {
		log.Fatal(err)
	}

	// if you want to mock the database
	// just use mDB above and set it here instead of dbs
	proto.RegisterServiceServer(srv, &handlers.Handler{DB:dbs})
	go func(){
		_ = srv.Serve(lis)
	}()

	// create a grpc dialer for client
	conn, _ = grpc.DialContext(context.Background(), "bufnet", grpc.WithDialer(bufDialer), grpc.WithInsecure())

	// initialize the client
	client = proto.NewServiceClient(conn)

	// define mocking db rules
	//mDB.On("Get", "user:1").Return(map[string]string{"json":`{"id":"1","name":"Cyan Moore","email":"cyanmoore@gmail.com","password":"testing123","username":"cyan-moore"}`}, nil)
	//mDB.On("Create", "user:2", `{"id":"2","name":"Daren Moore","email":"darenmoore@gmail.com","password":"testing123","username":"daren-moore"}`).Return(`{"id":"2","name":"Daren Moore","email":"darenmoore@gmail.com","password":"testing123","username":"daren-moore"}`, nil)
}

func bufDialer(string, time.Duration) (net.Conn, error) {
	return lis.Dial()
}

func TestInsertResource(t *testing.T) {
	t.Run("test-insert-user", func(t *testing.T) {
		resource := &proto.Resource{
			ResourceType: "user",
			User: &user.User{
				Id:                   "2",
				Name:                 "Daren Moore",
				Email:                "darenmoore@gmail.com",
				Password:             "testing123",
				Username:             "daren-moore",
			},
		}
		res, err := client.InsertResource(context.Background(), resource)
		if err != nil {
			t.Errorf(err.Error())
		} else if res.User == nil{
			t.Errorf("Empty data got")
		} else if res.User.Name != "Daren Moore" {
			t.Errorf("Wrong data got after insert")
		}
	})

	t.Run("test-insert-product", func(t *testing.T) {
		resource := &proto.Resource{
			ResourceType: "product",
			Product: &product.Product{
				Id:       "1",
				Name:     "Product A",
				Quantity: 200,
				Price:    50,
				OwnerInfo: &user.User{
					Id:                   "1",
					Name:                 "Cyan Moore",
				},
			},
		}
		res, err := client.InsertResource(context.Background(), resource)
		if err != nil {
			t.Errorf(err.Error())
		} else if res.Product == nil{
			t.Errorf("Empty data got")
		} else if res.Product.Name != "Product A" {
			t.Errorf("Wrong data got after insert")
		}
	})

	t.Run("test-insert-order", func(t *testing.T) {
		var resource = &proto.Resource{
			ResourceType: "order",
			Order: &order.Order{
				Id: "1",
				ProductInfo: &product.Product{
					Id:       "1",
					Name:     "Product A",
					Quantity: 1,
					Price:    200,
				},
				UserInfo: &user.User{
					Id:    "1",
					Name:  "Cyan Moore",
					Email: "cyanmoore@gmail.com",
				},
				Date: &timestamp.Timestamp{
					Seconds: int64(time.Now().Second()),
					Nanos:   int32(time.Now().Nanosecond()),
				},
				OwnerInfo: &user.User{
					Id:    "2",
					Name:  "Daren JMoore",
					Email: "darenmoore@gmail.com",
				},
			},
		}
		res, err := client.InsertResource(context.Background(), resource)
		if err != nil {
			t.Errorf(err.Error())
		} else if res.Order == nil{
			t.Errorf("Empty data got")
		} else if res.Order.ProductInfo.Name != "Product A" {
			t.Errorf("Wrong data got after insert")
		} else if res.Order.OwnerInfo.Name != "Daren JMoore" {
			t.Errorf("Wrong data got after insert")
		}
	})
}

func TestGetResource(t *testing.T) {
	t.Run("test-get-user", func(t *testing.T) {
		req := &proto.Request{
			ResourceType: "user",
			Id: "2",
		}
		res, err := client.GetResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		} else if res.User == nil {
			t.Errorf("Empty data got")
		} else if res.User.Name != "Daren Moore" {
			t.Errorf("Wrong data got")
		}
	})

	t.Run("test-get-user-false", func(t *testing.T) {
		req := &proto.Request{
			ResourceType: "user",
			Id: "10",
		}
		res, err := client.GetResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		} else if res.User != nil {
			t.Errorf("Wrong data got")
		}
	})

	t.Run("test-get-product", func(t *testing.T) {
		req := &proto.Request{
			ResourceType: "product",
			Id: "1",
		}
		res, err := client.GetResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		} else if res.Product == nil {
			t.Errorf("Empty data got")
		} else if res.Product.Name != "Product A" {
			t.Errorf("Wrong data got")
		}
	})

	t.Run("test-get-product-false", func(t *testing.T) {
		req := &proto.Request{
			ResourceType: "product",
			Id: "10",
		}
		res, err := client.GetResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		} else if res.Product != nil {
			t.Errorf("Wrong data got")
		}
	})

	t.Run("test-get-order", func(t *testing.T) {
		req := &proto.Request{
			ResourceType: "order",
			Id: "1",
		}
		res, err := client.GetResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		} else if res.Order == nil {
			t.Errorf("Empty data got")
		} else if res.Order.ProductInfo.Name != "Product A" {
			t.Errorf("Wrong data got")
		}
	})

	t.Run("test-get-order-false", func(t *testing.T) {
		req := &proto.Request{
			ResourceType: "order",
			Id: "10",
		}
		res, err := client.GetResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		} else if res.Order != nil {
			t.Errorf("Wrong data got")
		}
	})
}

func TestUpdateResource(t *testing.T) {
	t.Run("test-update-user", func(t *testing.T) {
		patchJson := []byte(`[{"op": "replace", "path": "/name", "value": "David Beckham"}]`)
		req := &proto.PatchRequest{
			ResourceType: "user",
			Id: "2",
			Patch: string(patchJson),
		}

		_, err := client.UpdateResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("test-update-user-again", func(t *testing.T) {
		patchJson := []byte(`[{"op": "replace", "path": "/name", "value": "Cyan Moore"}]`)
		req := &proto.PatchRequest{
			ResourceType: "user",
			Id: "2",
			Patch: string(patchJson),
		}

		_, err := client.UpdateResource(context.Background(), req)
		if err != nil {
			t.Fatal(err)
		}
	})
}
