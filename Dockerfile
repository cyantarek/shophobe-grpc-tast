FROM golang:latest

RUN mkdir -p /app
WORKDIR /app

ADD shophobe-grpc /app/shophobe-grpc

CMD ["./shophobe-grpc"]