package main

import (
	"gitlab.com/cyantarek/shophobe-grpc-tast/db"
	"gitlab.com/cyantarek/shophobe-grpc-tast/handlers"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)


// run redis inside docker
// or create a free cluster in scalegrid.io for free
// then use host, port and password like below

func main() {
	// initialize a new handler
	newHandler := new(handlers.Handler)
	// connect to database
	dB, err := db.NewRedisDb(os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT"), os.Getenv("DB_PASS"))
	if err != nil {
		panic(err)
	}

	newHandler.DB = dB

	// instantiate a tcp listener
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		panic(err)
	}

	// create a grpc server
	srv := grpc.NewServer()

	// register the server and handler
	proto.RegisterServiceServer(srv, newHandler)

	log.Println("gRPC server running on port :50051")

	// start the server
	log.Fatal(srv.Serve(lis))
}
