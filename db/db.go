package db

// DB defines the core database operations like: Get data, insert data
type DB interface {
	// Get accepts a key and returns a map structure filled with data.
	// It also returns an error
	Get(key string) (map[string]string, error)
	// Create accepts a key as string and json data as patch of string.
	// It then creates the data in database and returns it as string
	Create(key, json string) (string, error)
}