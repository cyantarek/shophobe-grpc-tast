package db

import (
	"fmt"
	"github.com/mediocregopher/radix.v2/redis"
	"log"
	"time"
)

// redisDb implements the DB interface
type redisDb struct {
	// client is the pointer to the redis client instance
	client *redis.Client
}

// NewRedisDb accepts uri string and returns a redisDb instance
func NewRedisDb(uri, pass string) (DB, error) {
	fmt.Println(uri)
	conn, err := redis.DialTimeout("tcp", uri, time.Second*15)
	if err != nil {
		return nil, err
	}

	fmt.Println("database connected")

	r := conn.Cmd("AUTH", pass)
	if r.Err != nil {
		log.Fatal("AUTH Failed:", r.Err)
	}

	return &redisDb{client:conn}, nil
}

func (r *redisDb) Get(key string) (map[string]string, error) {
	// execute HGETALL command with specified key
	// returns a Map() a.k.a map[string]string
	m, err := r.client.Cmd("HGETALL", key).Map()
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (r *redisDb) Create(key, json string) (string, error) {
	// creates data by executing HMSET command
	// stores the whole json doc as string
	r.client.Cmd("HMSET", key, "json", json)

	// finally returns the inserted data as string
	// that can be decoded to struct later
	return r.client.Cmd("HGET", key, "json").Str()
}