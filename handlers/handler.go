package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"gitlab.com/cyantarek/shophobe-grpc-tast/db"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto/order"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto/product"
	"gitlab.com/cyantarek/shophobe-grpc-tast/proto/user"
	jsonpatch "gopkg.in/evanphx/json-patch.v4"
	"log"
)

// handler is the generic handler that holds the DB instance
type Handler struct {
	DB db.DB
}

// GetResource is a gRPC function attached to the handler
// it returns resource based on resource type and id
func (h *Handler) GetResource(ctx context.Context, req *proto.Request) (*proto.Response, error) {
	// gone through the ResourceType field in order to identify
	// what data user expects between product, user and order
	switch req.ResourceType {
	case "product":
		// queries the data from DB
		res, err := h.DB.Get("product:" + req.Id)
		if err != nil {
			log.Panic(err)
		}
		var result *product.Product
		// unmarshal the string patch to struct
		_ = json.Unmarshal([]byte(res["json"]), &result)
		return &proto.Response{Product: result}, nil

	// other things are same as above
	case "order":
		res, err := h.DB.Get("order:" + req.Id)
		if err != nil {
			log.Panic(err)
		}
		var result *order.Order
		_ = json.Unmarshal([]byte(res["json"]), &result)
		return &proto.Response{Order: result}, nil
	case "user":
		res, err := h.DB.Get("user:" + req.Id)
		if err != nil {
			log.Panic(err)
		}

		var result *user.User
		_ = json.Unmarshal([]byte(res["json"]), &result)

		return &proto.Response{User: result}, nil
	default:
		// default returns an error message if the Request is invalid
		return &proto.Response{Error: errors.New("no data found for that query").Error()}, nil
	}
	return nil, nil
}

func (h *Handler) UpdateResource(ctx context.Context, req *proto.PatchRequest) (*proto.Resource, error) {
	switch req.ResourceType {
	case "product":
		// get the original data based on id
		original, err := h.DB.Get("product:" + req.Id)
		if err != nil {
			log.Panic(err)
		}

		var result *product.Product
		_ = json.Unmarshal([]byte(original["json"]), &result)
		byteJson, _ := json.Marshal(result)
		patchJson := []byte(req.Patch)

		// decode json patch
		patch, err := jsonpatch.DecodePatch(patchJson)
		if err != nil {
			log.Panic(err)
		}

		// apply patch
		modified, err := patch.Apply(byteJson)
		if err != nil {
			log.Panic(err)
		}

		var productR *product.Product
		// update DB entry by overwriting it
		resStr, _ := h.DB.Create("product:" + req.Id, string(modified))
		err = json.Unmarshal([]byte(resStr), &productR)
		if err != nil {
			panic(err)
		}
		return &proto.Resource{Product: productR}, nil

	case "order":
		original, err := h.DB.Get("order:" + req.Id)
		if err != nil {
			log.Panic(err)
		}

		var result *order.Order
		_ = json.Unmarshal([]byte(original["json"]), &result)
		byteJson, _ := json.Marshal(result)
		patchJson := []byte(req.Patch)

		patch, err := jsonpatch.DecodePatch(patchJson)
		if err != nil {
			log.Panic(err)
		}

		modified, err := patch.Apply(byteJson)
		if err != nil {
			log.Panic(err)
		}

		var orderR *order.Order
		resStr, _ := h.DB.Create("order:" + req.Id, string(modified))
		err = json.Unmarshal([]byte(resStr), &orderR)
		if err != nil {
			panic(err)
		}
		return &proto.Resource{Order: orderR}, nil
	case "user":
		original, err := h.DB.Get("user:" + req.Id)
		if err != nil {
			log.Panic(err)
		}

		var result *user.User
		_ = json.Unmarshal([]byte(original["json"]), &result)
		byteJson, _ := json.Marshal(result)
		patchJson := []byte(req.Patch)

		patch, err := jsonpatch.DecodePatch(patchJson)
		if err != nil {
			log.Panic(err)
		}

		modified, err := patch.Apply(byteJson)
		if err != nil {
			log.Panic(err)
		}

		var userR *user.User
		resStr, _ := h.DB.Create("user:" + req.Id, string(modified))
		err = json.Unmarshal([]byte(resStr), &userR)
		if err != nil {
			panic(err)
		}
		return &proto.Resource{User: userR}, nil
	}
	return nil, nil
}

func (h *Handler) InsertResource(ctx context.Context, res *proto.Resource) (*proto.Resource, error) {
	switch res.ResourceType {
	case "product":
		jsonByte, _ := json.Marshal(res.Product)
		resStr, _ := h.DB.Create("product:" + res.Product.Id, string(jsonByte))

		var productR *product.Product
		err := json.Unmarshal([]byte(resStr), &productR)
		if err != nil {
			panic(err)
		}
		return &proto.Resource{Product: productR}, nil
	case "order":
		jsonByte, _ := json.Marshal(res.Order)
		resStr, _ := h.DB.Create("order:" + res.Order.Id, string(jsonByte))

		var orderR *order.Order
		err := json.Unmarshal([]byte(resStr), &orderR)
		if err != nil {
			panic(err)
		}
		return &proto.Resource{Order: orderR}, nil
	case "user":
		jsonByte, _ := json.Marshal(res.User)
		resStr, _ := h.DB.Create("user:" + res.User.Id, string(jsonByte))

		var userR *user.User
		err := json.Unmarshal([]byte(resStr), &userR)
		if err != nil {
			panic(err)
		}
		// set the password to empty before returning response
		userR.Password = ""
		return &proto.Resource{User: userR}, nil
	}
	return nil, nil
}
